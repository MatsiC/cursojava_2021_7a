-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.7.13-log


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema maxi_camacho
--

CREATE DATABASE IF NOT EXISTS maxi_camacho;
USE maxi_camacho;

--
-- Definition of table `pantallas`
--

DROP TABLE IF EXISTS `pantallas`;
CREATE TABLE `pantallas` (
  `PAN_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PAN_DESCRIPCION` varchar(50) NOT NULL,
  PRIMARY KEY (`PAN_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `pantallas`
--

/*!40000 ALTER TABLE `pantallas` DISABLE KEYS */;
INSERT INTO `pantallas` (`PAN_ID`,`PAN_DESCRIPCION`) VALUES 
 (1,'ABM ALUMNOS'),
 (2,'ABM PROFESORES'),
 (3,'ABM EVALUACIONES'),
 (4,'REALIZAR EVALUACIONES'),
 (5,'ABM USUARIOS'),
 (6,'VISUALIZAR INFORMES');
/*!40000 ALTER TABLE `pantallas` ENABLE KEYS */;


--
-- Definition of table `partidos`
--

DROP TABLE IF EXISTS `partidos`;
CREATE TABLE `partidos` (
  `PAR_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PRO_ID` int(10) unsigned NOT NULL,
  `PAR_DESCRIPCION` varchar(50) NOT NULL,
  PRIMARY KEY (`PAR_ID`),
  KEY `FK_Provincias` (`PRO_ID`),
  CONSTRAINT `FK_Provincias` FOREIGN KEY (`PRO_ID`) REFERENCES `provincias` (`PRO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partidos`
--

/*!40000 ALTER TABLE `partidos` DISABLE KEYS */;
INSERT INTO `partidos` (`PAR_ID`,`PRO_ID`,`PAR_DESCRIPCION`) VALUES 
 (1,1,'EL MEJOR'),
 (2,1,'Bahia Blanca'),
 (3,1,'San Pedro'),
 (4,1,'Ituzaingo'),
 (5,2,'Calamuchita'),
 (6,2,'Santa Maria'),
 (7,2,'Rio Primero'),
 (8,2,'Rio Seco'),
 (9,4,'Colon'),
 (10,4,'Gualeguay'),
 (11,4,'Gualeguaychu'),
 (12,4,'La Paz');
/*!40000 ALTER TABLE `partidos` ENABLE KEYS */;


--
-- Definition of table `perfiles`
--

DROP TABLE IF EXISTS `perfiles`;
CREATE TABLE `perfiles` (
  `PER_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PER_DESCRIPCION` varchar(50) NOT NULL,
  PRIMARY KEY (`PER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `perfiles`
--

/*!40000 ALTER TABLE `perfiles` DISABLE KEYS */;
INSERT INTO `perfiles` (`PER_ID`,`PER_DESCRIPCION`) VALUES 
 (1,'ALUMNOS'),
 (2,'PROFESORES'),
 (3,'ADMINISTRADOR');
/*!40000 ALTER TABLE `perfiles` ENABLE KEYS */;


--
-- Definition of table `perfiles_pantallas`
--

DROP TABLE IF EXISTS `perfiles_pantallas`;
CREATE TABLE `perfiles_pantallas` (
  `PER_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PAN_ID` int(10) unsigned NOT NULL,
  KEY `FK_PERFILES` (`PER_ID`),
  KEY `FK_PANTALLAS` (`PAN_ID`),
  CONSTRAINT `FK_PANTALLAS` FOREIGN KEY (`PAN_ID`) REFERENCES `pantallas` (`PAN_ID`),
  CONSTRAINT `FK_PERFILES` FOREIGN KEY (`PER_ID`) REFERENCES `perfiles` (`PER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `perfiles_pantallas`
--

/*!40000 ALTER TABLE `perfiles_pantallas` DISABLE KEYS */;
INSERT INTO `perfiles_pantallas` (`PER_ID`,`PAN_ID`) VALUES 
 (1,1),
 (1,4),
 (2,2),
 (2,3),
 (3,1),
 (3,2),
 (3,3),
 (3,4),
 (3,5),
 (3,6);
/*!40000 ALTER TABLE `perfiles_pantallas` ENABLE KEYS */;


--
-- Definition of table `provincias`
--

DROP TABLE IF EXISTS `provincias`;
CREATE TABLE `provincias` (
  `PRO_ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PRO_DESCRIPCION` varchar(50) NOT NULL,
  PRIMARY KEY (`PRO_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provincias`
--

/*!40000 ALTER TABLE `provincias` DISABLE KEYS */;
INSERT INTO `provincias` (`PRO_ID`,`PRO_DESCRIPCION`) VALUES 
 (1,'Buenos Aires'),
 (2,'CBA'),
 (4,'Buenos Mates');
/*!40000 ALTER TABLE `provincias` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
