package modulo1;

import java.util.Scanner;

public class EjemploDeCiclos {
	public static void main(String[] args) {
		
		System.out.println("Ingrese una tabla a evaluar");
		Scanner scan = new Scanner (System.in);
		
		int tabla = scan.nextInt();
		
		System.out.println("la tabla a mostrar es"+tabla);
		//inicio; condicion; incremente i = i+1
		//la cantidad de ciclos es conocido
		
		for(int i=0; i<=10; i++){
			int result = tabla * i;
			System.out.println(tabla + "x" + i + "=" + result);
			
			
		}
		
		//voy a ingresar valores hasta el primer numero par
		
		System.out.println("ingrese valor, si es par se termina");
		int valor = scan.nextInt();		//ingreso un valor con el teclado
		int resto = valor %2;			//divido por dos el valor y me da el resto
		while(resto == 1){				//lo de abajo se ejecuta mientras el resto = 1
			System.out.println("ingrese un valor si es par se corta");
			
			valor = scan.nextInt();		//para seguir el ciclo
			resto = valor %2;			//lo mismo q arriba
			
		}
		
		scan.close();
		
	}

}
