package modulo1;

import java.util.Scanner;

public class EjemploDeSwitch {
public static void main(String[] args) {
	System.out.println("Ingrese el resultado de la competencia");
	Scanner scan = new Scanner(System.in);
	
	int posicion = scan.nextInt();
	
	switch (posicion) {
	
	case 1:
		System.out.println("Bien ahi");
		break;
	case 2: 
		System.out.println("bueno, casi ");
		break;
	case 3:
		System.out.println("nada mal, medalla de bronce");
		break;
		
	default:
		System.out.println("al menos lo intentaste arriba esos animos");
		break;
		}
	
	scan.close();
	
}

}
