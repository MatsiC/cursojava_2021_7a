package modulo1;

import java.util.Scanner;

public class Ejercicio4 {
public static void main(String[] args) {
	
	Scanner scan = new Scanner(System.in);
	System.out.println("ingrese tres notas");
	
	float nota1=scan.nextFloat();
	float nota2=scan.nextFloat();
	float nota3=scan.nextFloat();

	float promedio = (nota1+nota2+nota3)/3;
	System.out.println("nota1=" + nota1);
	System.out.println("nota2=" + nota2);
	System.out.println("nota3=" + nota3);
	
	if(promedio >=7)
		System.out.println("aprobado con un promedio de " + promedio);
	else
		System.out.println("desaprobo con un promedio de" + promedio);
	
	scan.close();
	
}
}
