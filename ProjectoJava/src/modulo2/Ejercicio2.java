package modulo2;

public class Ejercicio2 {
	
	public static void main(String[] args) {
		
		byte	bmin = -128;
		byte	bmax = 127;
		short	smin = -32768;
		short	smax = 32767;
		int 	imin = -2147483648;
		int 	imax = 2147483647;
		long	lmin = -9223372036854775808L;
		long 	lmax = 9223372036854775807L;
		
		
		System.out.println("tipo\t\tminimo\t\t\tmaximo");
		System.out.println("....\t\t......\t\t\t......");
		System.out.println("\nbyte\t\t"+ bmin + "\t\t\t" + bmax);
		System.out.println("\nshort\t\t" + smin + "\t\t\t" + smax);
		System.out.println("\nint\t\t" + imin + "\t\t" + imax);
		System.out.println("\nlong\t\t" + lmin + "\t" + lmax);
		System.out.println("\n\n\t\t La formula seria: \t  2^(n-1)   para el numero maximo "
				+ "\n\t\t\t\t y\t -2^n para el numero minimo");
	}

}
