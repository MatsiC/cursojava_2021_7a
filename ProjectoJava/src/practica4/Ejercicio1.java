package practica4;
import java.util.Scanner;


public class Ejercicio1 {
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Nota 1?\n");
		int nota1 = scan.nextByte(); 
		
		System.out.println("Nota 2?\n");
		int nota2 = scan.nextByte();
		
		System.out.println("Nota 3?\n");
		int nota3 = scan.nextByte();
		
		int promedio = (nota1+nota2+nota3)/3;
		
		System.out.println("El promedio es " + promedio + "\n");
		
		if (promedio<7)
			System.out.println("Y el alumno esta desaprobado");
		else System.out.println("Y el alumno esta aprobado");
		     
				
	scan.close();
		
		
		
	}

}
