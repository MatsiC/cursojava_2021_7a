package practica4;

import java.util.Scanner;

public class Ejercicio10 {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Ingrese tres variables");
		
		int valor1 = scan.nextInt();
		int valor2 = scan.nextInt();
		int valor3 = scan.nextInt();
		
		if (valor1>valor2 && valor1>valor3) System.out.println("El mayor valor es " + valor1);
		if (valor2>valor1 && valor2>valor3) System.out.println("El mayor valor es " + valor2);
		if (valor3>valor1 && valor3>valor2) System.out.println("El mayor valor es " + valor3);
		
		if (valor1 == valor2 && valor1>valor3) System.out.println("El mayor valor es " + valor1);
		if (valor1 == valor3 && valor1>valor2) System.out.println("El mayor valor es " + valor1);
		if (valor2 == valor3 && valor2>valor1) System.out.println("El mayor valor es " + valor2);
		
		if (valor1 == valor2 && valor2 == valor3) System.out.println("Son iguales");
		
		scan.close();
		
	}

}
