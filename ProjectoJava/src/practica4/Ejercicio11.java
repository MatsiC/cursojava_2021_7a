package practica4;

import java.util.Scanner;

public class Ejercicio11 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("DECIME UNA LETRA\n");
		
		char letra = scan.next().charAt(0);
				
		if ((letra == 'a') || (letra == 'e') || (letra == 'i') || (letra == 'o') || (letra == 'u')){
			
			System.out.println("ES VOCAL");
		}
		
		else System.out.println("ES CONSONANTE");
	
		scan.close();
		
	}
}
