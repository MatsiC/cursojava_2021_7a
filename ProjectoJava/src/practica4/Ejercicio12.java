package practica4;

import java.util.Scanner;

public class Ejercicio12 {

	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);
		
		System.out.println("INGRESAR UN NUMERO");
		int numero = scan.nextInt();
		
		if (numero>0 && numero <13) System.out.println("Corresponde a la primer docena");
		if (numero>12 && numero <25) System.out.println("Corresponde a la segunda docena");
		if (numero>24 && numero <37) System.out.println("Corresponde a la tercera docena");
		if ((numero<1) || (numero>36)) System.out.println("El numero " + numero + " esta fuera de rango");
		
		scan.close();
 	}
}
