package practica4;

import java.util.Scanner;

public class Ejercicio15 {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("DECIME LA CLASE\n");
		
		char clase = scan.next().charAt(0);
		
		switch (clase){
		
		case 'a':
			System.out.println("4 ruedas y un motor");
		break;
		
		case 'b':
			System.out.println("4 ruedas, motor, cierre centralizado y aire acondicionado");
		break;
		
		case 'c':
			System.out.println("4 ruedas, un motor, cierre centralizado, aire y airbag");
		break;
			
		}
		
		scan.close();
	}

}
