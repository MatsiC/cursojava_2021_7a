package practica4;

import java.util.Scanner;

public class Ejercicio16 {

	public static void main(String[] args) {
		
		int sumapar = 0;
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("QUE TABLA QUERES QUE CALCULE");
		
		int tabla = scan.nextInt();
		
		for(int i=0; i<=10; i++){
			int result = tabla * i;
			System.out.println(tabla + "x" + i + "=" + result);
			
			int resto = result %2;
			
			if (resto == 0){
				
				sumapar = sumapar + result;
			}
			
		
	}
		
		System.out.println("La suma de los resultados pares es " + sumapar);
		
		scan.close();
}
}