package practica4;

import java.util.Scanner;

public class Ejercicio18 {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("HOLA QUERES QUE TE DIGA TODAS LAS TABLAS?");
		
		String respuesta = scan.nextLine();
		
		if (respuesta.equals("si")){
			System.out.println("Ya te digo...\n");
		}
		
		if (respuesta.equals("no")){
			System.out.println("Bueno a mi que me importa te las digo igual");
		}
		

		
		for(int i=0 ; i<=10 ; i++){
			
			for(int j=0 ; j<=10 ; j++){
				
				int resultado = i * j;
				
				System.out.println( i + " x " + j + "= " + resultado);
			}
		}
		
		scan.close();
	}

}
