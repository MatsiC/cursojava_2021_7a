package practica4;

import java.util.Scanner;

public class Ejercicio19 {
	public static void main(String[] args) {
		
		int suma = 0;
		
		
		Scanner scan = new Scanner (System.in);
		
		int contador = 0;
		
		while (contador < 10){
			
			int random = (int) Math.floor(Math.random()*10 + 1);
			
			System.out.println(random + "\n");
			
			contador = contador + 1;
			
			suma = suma + random;
		}
		
		int promedio = suma/10;
		
		System.out.println("La suma de todos los valores es " + suma +"\ny el promedio es " + promedio);
		
		scan.close();
		
	}
	
	
}
