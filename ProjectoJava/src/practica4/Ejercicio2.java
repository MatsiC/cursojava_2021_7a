package practica4;

import java.util.Scanner;


public class Ejercicio2 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Ingresa un numero, yo te digo si es par o no");
		
		int numero = scan.nextInt();
		
		int resto = numero % 2;
		
		if (resto!=0)
			System.out.println("ES IMPAR");
		else System.out.println("ES PAR");
		
		scan.close();
		
			
		
		
	}
}
