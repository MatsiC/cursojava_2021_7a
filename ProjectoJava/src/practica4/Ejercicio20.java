package practica4;


public class Ejercicio20 {
	
	public static void main(String[] args) {
		
		int maximo = -9999;
		int minimo = 9999;
		int contador = 0;
		
		do{
			int random = (int) Math.floor(Math.random()*10 + 1);
			
			System.out.println(random + "\n");
			
			contador = contador + 1;
			
			if (random > maximo) maximo = random; 
			if (random < minimo) minimo = random;

			
		} while (contador<10);
		
		System.out.println("El valor minimo es " + minimo + " y el maximo es " + maximo);
	}

}
