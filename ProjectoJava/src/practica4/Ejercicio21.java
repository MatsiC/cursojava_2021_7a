package practica4;

import java.util.Scanner;

public class Ejercicio21 {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Ingrese su categoria");
		char categoria = scan.next().charAt(0);
		
		System.out.println("Ingrese su sueldo");
		int sueldo = scan.nextInt();
		
		System.out.println("Ingrese su antiguedad (a�os trabajando no de vida)");
		int antiguedad = scan.nextInt();
		
		if (antiguedad > 0 && antiguedad < 6 ){
			sueldo = (int) (sueldo + (0.05*sueldo));
		}
		
		else if (antiguedad > 5 && antiguedad < 11){
			sueldo = (int) (sueldo + (0.1*sueldo));
		}
		
		else if (antiguedad > 10){
			sueldo = (int) (sueldo + (0.3*sueldo));
		}
	
		
		
		if (categoria == 'a'){
			sueldo = sueldo + 1000;
	}
		else if (categoria == 'b'){
			sueldo = sueldo + 2000;
		}
		
		else if (categoria == 'c'){
			sueldo = sueldo + 3000;
		}
		
		
		
		System.out.println("Su sueldo bruto es de " + sueldo);
		scan.close();
}
	
}
