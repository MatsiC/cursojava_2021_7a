package practica4;

import java.util.Scanner;

public class Ejercicio3 {
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Dime el numero de un mes y te dire cuantos dias tiene\n");
		byte mes = scan.nextByte();
		
		if ((mes == 4) || (mes==6) || (mes==9)){
			System.out.println("Este mes tiene 30 dias");
		}
		else if (mes == 2) {
			System.out.println("Este mes tiene 28 dias");
		}
		else System.out.println("Este mes tiene 31 dias");
		
	
		scan.close();
		
	}

}
