package practica4;

import java.util.Scanner;

public class Ejercicio5 {
	
	public static void main(String[] args) {
		
		System.out.println("Ingrese el puesto en el torneo");
		Scanner scan = new Scanner(System.in);
		
		int puesto = scan.nextInt();
		
		if (puesto==1) System.out.println("Felicitaciones, medalla de oro!!");
		else if (puesto==2) System.out.println("Enhorabuena, medalla de plata!!");
		else if (puesto==3) System.out.println("�Que maravilla!, medalla de bronce");
		else System.out.println("Fue un buen intento! Arriba esos animos");
		
		scan.close();
	}

}
