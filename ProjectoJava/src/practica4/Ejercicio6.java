package practica4;

import java.util.Scanner;

public class Ejercicio6 {

	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Ingresar curso");
		int curso = scan.nextInt();
		
		if (curso<7 && curso>0) System.out.println("Primaria");
		else if (curso<13 && curso>6)	System.out.println("Secundaria");
		else if (curso == 0) System.out.println("Jardin de infantes");
		
		scan.close();
		
	}
}
