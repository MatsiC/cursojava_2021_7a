package practica4;

import java.util.Scanner;


public class Ejercicio8 {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Jugador 1\nPiedra = 0\nPapel = 1\nTijera = 2");
		int jugador1 = scan.nextInt();
		
		System.out.println("Jugador 2\nPiedra = 0\nPapel = 1\nTijera = 2");
		int jugador2 = scan.nextInt();
		
		if ((jugador1 == 0  && jugador2 == 2) || 
			(jugador1 == 1 && jugador2 == 0) || 
			(jugador1 == 2 && jugador2 == 1)) 

		System.out.println("Gana jugador 1");
		
		else if ((jugador2 == 0  && jugador1 == 2) || 
			(jugador2 == 1 && jugador1 == 0) || 
			(jugador2 == 2 && jugador1 == 1))
			
		System.out.println("Gana jugador 2");
	
		else System.out.println("Empate");
		
		
		scan.close();
	}

	
}
