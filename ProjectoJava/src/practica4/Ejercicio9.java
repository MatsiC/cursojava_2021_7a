package practica4;

import java.util.Scanner;

public class Ejercicio9 {
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Jugador 1\nIngrese su eleccion\nPiedra = 0\nPapel = 1\nTijera = 2");
		int jugador1 = scan.nextInt();
		
		System.out.println("Jugador 2\nIngrese su eleccion\nPiedra = 0\nPapel = 1\nTijera = 2");
		int jugador2 = scan.nextInt();
		
		if (jugador1==0){
			
			if (jugador2 == 2) System.out.println("Gana jugador 1");
			else if (jugador2 == 0) System.out.println("Empate");
			else System.out.println("Gana jugador 2");
		}
			
		if (jugador1 == 1){
			
			if (jugador2 == 0) System.out.println("Gana jugador 1");
			else if (jugador2 == 1) System.out.println("Empate");
			else System.out.println("Gana jugador 2");
		}

		if (jugador1 == 2){
			
			if (jugador2 == 1) System.out.println("Gana jugador 1");
			else if (jugador2 == 2) System.out.println("Empate");
			else System.out.println("Gana jugador 2");
		}
		
		scan.close();
		
	}
}
